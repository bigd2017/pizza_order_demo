require( 'sinatra' )
require( 'sinatra/contrib/all' )
require( 'pry-byebug' )

require_relative( './models/pizza_order' )
also_reload( './models/*' )

# ask class what HTTP verb should be used for index
get '/pizza-orders' do # index
  @orders = PizzaOrder.all()
  erb(:index)
end

#above 'show' route so that new route 'picks-up' id to displays and avoids
# treating :id as string when searching db.
get '/pizza-orders/new' do  # new
  erb (:new)
end

# use params[:id] in the block that defines the route.
get '/pizza-orders/:id' do  # show
  @order = PizzaOrder.find( params[:id])
  erb(:show)
end

# Now the create route passes this params hash to PizzaOrder.new(),
# and save. Note: the 'initialize' method used to initialise and store
# params from form includes ':id' params but,e.g. 'topping', 'first-name'...
 # 'passed' to our 'POST' route for display!

post '/pizza-orders' do  # create
  @order = PizzaOrder.new(params)
  @order.save()
  erb (:create)
end

# #sinatra reads top-down. update method accepts :id from create or edit
# route-handler accordingly
get '/pizza-orders/:id/edit' do # edit '/pizza-orders/5/edit'
  @order = PizzaOrder.find(params[:id])
  erb (:edit)
end

post '/pizza-orders/:id' do # update
  PizzaOrder.new( params ).update
  redirect to '/pizza-orders'
end

post '/pizza-orders/:id/delete' do # delete
  order = PizzaOrder.find( params[:id] )
  order.delete()
  redirect to '/pizza-orders'
end
